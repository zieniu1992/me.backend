﻿using AutoMapper;
using FluentAssertions;
using MediaExpert.Api.Controllers.v1;
using MediaExpert.Application.Functions.Products.Commands.CreateProduct;
using MediaExpert.Application.Functions.Products.Queries.GetProducts;
using MediaExpert.Application.Mapper.Dtos;
using MediaExpert.Application.Mapper.Dtos.Products;
using MediaExpert.Application.Mapper.Profiles;
using MediaExpert.Test.Applications.Mocks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;

namespace MediaExpert.Test.Applications.Controllers
{
    public class GetProductsControllerTests
    {
        private readonly ProductController _productController;

        public GetProductsControllerTests()
        {
            var configurationProvider = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProductProfile>();
            });

            var productRepository = RepositoryMocks.GetProductRepository().Object;

            var mapper = configurationProvider.CreateMapper();
            var mediator = new Mock<IMediator>();

            var getProductsHandler = new GetProductsQueryHandler(productRepository, mapper);

            mediator.Setup(s => s.Send(It.IsAny<GetProductsQuery>(),
                                            It.IsAny<CancellationToken>())).Returns(getProductsHandler.Handle);

            _productController = new ProductController(mediator.Object);
        }

        [Fact]
        public async Task CreateProductControllerSuccessTest()
        {
            var result = await _productController.GetAll();
            var objectResult = result.Result as OkObjectResult;
            var products = objectResult.Value as List<ProductDto>;

            products.Should().NotBeEmpty();
            products.Count.Should().Be(5);            
        }
    }
}
