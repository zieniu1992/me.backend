﻿using AutoMapper;
using FluentAssertions;
using MediaExpert.Api.Controllers.v1;
using MediaExpert.Application.Functions.Products.Commands.CreateProduct;
using MediaExpert.Application.Interfaces.Persistence.Repositories.Products;
using MediaExpert.Application.Mapper.Dtos;
using MediaExpert.Application.Mapper.Profiles;
using MediaExpert.Test.Applications.Mocks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;

namespace MediaExpert.Test.Applications.Controllers
{
    public class CreateProductControllerTests
    {
        private readonly ProductController _productController;

        public CreateProductControllerTests()
        {
            var configurationProvider = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProductProfile>();
            });

            var productRepository = RepositoryMocks.GetProductRepository().Object;

            var mapper = configurationProvider.CreateMapper();
            var mediator = new Mock<IMediator>();

            var createProductHandler = new CreateProductCommandHandler(productRepository, mapper);

            mediator.Setup(s => s.Send(It.IsAny<CreateProductCommand>(),
                                            It.IsAny<CancellationToken>())).Returns(createProductHandler.Handle);

            _productController = new ProductController(mediator.Object);
        }

        [Fact]
        public async Task CreateProductControllerSuccessTest()
        {
            var product = new CreateProductCommand("4124125125216", "Testowe7", 450);

            var result = await _productController.Create(product);
            var objectResult = result.Result as OkObjectResult;
            var idsDto = objectResult.Value as IdsDto;

            idsDto.Id.Should().NotBe(Guid.Empty);
            idsDto.Id.Should().NotBeEmpty();
        }

        [Fact]
        public async Task CreateProductControllerNullObjectTest()
        {
            var result = await _productController.Create(null);
            var objectResult = result.Result as BadRequestResult;

            objectResult.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
        }        
        
        [Fact]
        public async Task CreateProductControllerNoValidTest()
        {
            var product = new CreateProductCommand("", "", -8);
            var result = await _productController.Create(product);
            var objectResult = result.Result as BadRequestResult;

            objectResult.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
        }  
        
        [Fact]
        public async Task CreateProductControllerExistInDatabaseTest()
        {
            var product = new CreateProductCommand("4124125125211", "Testowe123", 8);
            var result = await _productController.Create(product);
            var objectResult = result.Result as ConflictResult;

            objectResult.StatusCode.Should().Be(StatusCodes.Status409Conflict);
        }
    }
}