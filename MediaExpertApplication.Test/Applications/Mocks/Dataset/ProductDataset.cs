﻿using MediaExpert.Domain.Entities.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaExpert.Test.Applications.Mocks.Dataset
{
    public class ProductDataset
    {
        public static List<Product> GetProducts => new()
        {
            new()
            {
                Id = Guid.NewGuid(),
                Code = "4124125125211",
                Created = DateTimeOffset.UtcNow,
                Modified = DateTimeOffset.UtcNow,
                Name = "Testowe1",
                Price = 10
            },
            new()
            {
                Id = Guid.NewGuid(),
                Code = "4124125125212",
                Created = DateTimeOffset.UtcNow,
                Modified = DateTimeOffset.UtcNow,
                Name = "Testowe2",
                Price = 15
            },
            new()
            {
                Id = Guid.NewGuid(),
                Code = "4124125125213",
                Created = DateTimeOffset.UtcNow,
                Modified = DateTimeOffset.UtcNow,
                Name = "Testowe3",
                Price = 12
            },
            new()
            {
                Id = Guid.NewGuid(),
                Code = "4124125125214",
                Created = DateTimeOffset.UtcNow,
                Modified = DateTimeOffset.UtcNow,
                Name = "Testowe4",
                Price = 16
            },
            new()
            {
                Id = Guid.NewGuid(),
                Code = "4124125125215",
                Created = DateTimeOffset.UtcNow,
                Modified = DateTimeOffset.UtcNow,
                Name = "Testowe5",
                Price = 18
            },
        };
    }
}
