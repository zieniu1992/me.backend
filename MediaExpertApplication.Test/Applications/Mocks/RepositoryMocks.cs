﻿using MediaExpert.Application.Interfaces.Persistence.Repositories.Products;
using MediaExpert.Domain.Entities.Products;
using MediaExpert.Domain.Status;
using MediaExpert.Test.Applications.Mocks.Dataset;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaExpert.Test.Applications.Mocks
{
    public class RepositoryMocks
    {
        public static Mock<IProductRepository> GetProductRepository()
        {
            var products = ProductDataset.GetProducts;
            var mockProductRepository = new Mock<IProductRepository>();

            mockProductRepository.Setup(s => s.AddAsync(It.IsAny<Product>())).ReturnsAsync((Product product) =>
            {
                product.Id = Guid.NewGuid();
                product.Created = DateTimeOffset.Now;
                product.Modified = DateTimeOffset.Now;
                products.Add(product);
                return ExecutionStatus<Product>.DbOk(product);
            });

            mockProductRepository.Setup(s => s.GetAllAsync()).ReturnsAsync(() => ExecutionStatus<IReadOnlyList<Product>>.DbOk(products));
            mockProductRepository.Setup(s => s.GetProductByCodeAsync(It.IsAny<string>())).ReturnsAsync((string code) =>
            {
                var product = products.FirstOrDefault(s => s.Code == code);

                return ExecutionStatus<Product>.DbIfDefaultThenError(product);
            });

            return mockProductRepository;
        }
    }
}
