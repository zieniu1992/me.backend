﻿using AutoMapper;
using FluentAssertions;
using MediaExpert.Application.Functions.Products.Commands.CreateProduct;
using MediaExpert.Application.Functions.Products.Queries.GetProducts;
using MediaExpert.Application.Interfaces.Persistence.Repositories.Products;
using MediaExpert.Application.Mapper.Dtos.Products;
using MediaExpert.Application.Mapper.Profiles;
using MediaExpert.Test.Applications.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaExpert.Test.Applications.Functions.Products.Queries
{
    public class GetProductsTests
    {
        private readonly GetProductsQueryHandler _getProductsHandler;
        private readonly IProductRepository _productRepository;

        public GetProductsTests()
        {
            var configurationProvider = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProductProfile>();
            });

            var mapper = configurationProvider.CreateMapper();

            _productRepository = RepositoryMocks.GetProductRepository().Object;

            _getProductsHandler = new GetProductsQueryHandler(_productRepository, mapper);
        }

        [Fact]
        public async Task GetProductsSuccessFiveObjectTest()
        {
            var result = await _getProductsHandler.Handle(new GetProductsQuery(), CancellationToken.None);

            result.Success.Should().BeTrue();
            result.Products.Should().BeOfType<List<ProductDto>>();
            result.Products.Count.Should().Be(5);
        }
    }
}
