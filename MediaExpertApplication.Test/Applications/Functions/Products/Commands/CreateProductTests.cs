﻿using AutoMapper;
using FluentAssertions;
using FluentAssertions.Equivalency;
using MediaExpert.Application.Common;
using MediaExpert.Application.Functions.Products.Commands.CreateProduct;
using MediaExpert.Application.Interfaces.Persistence.Repositories.Products;
using MediaExpert.Application.Mapper.Profiles;
using MediaExpert.Persistence.Ef.Repositories.Products;
using MediaExpert.Test.Applications.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MediaExpert.Test.Applications.Functions.Products.Commands
{
    public class CreateProductTests
    {
        private readonly CreateProductCommandHandler _createProductHandler;
        private readonly IProductRepository _productRepository;

        public CreateProductTests()
        {
            var configurationProvider = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProductProfile>();
            });

            var mapper = configurationProvider.CreateMapper();

            _productRepository = RepositoryMocks.GetProductRepository().Object;

            _createProductHandler = new CreateProductCommandHandler(_productRepository, mapper);
        }

        [Fact]
        public async Task CreateProductSuccessTest()
        {
            var product = new CreateProductCommand("4124125125216", "Testowe7", 450);

            var productsBefore = await _productRepository.GetAllAsync();
            var productsBeforeCount = productsBefore.Value.Count;

            var result = await _createProductHandler.Handle(product, CancellationToken.None);

            var productsAfter = await _productRepository.GetAllAsync();
            var productsAfterCount = productsAfter.Value.Count;

            result.ValidationErrors.Should().BeEmpty();
            result.Success.Should().BeTrue();

            productsBeforeCount.Should().BeLessThan(productsAfterCount);
        }

        [Theory]
        [InlineData("","Testowe10",888)]
        [InlineData("4124125125113", "",888)]
        [InlineData("4124125125113", "Testowe10", -50)]
        [InlineData("4124125125113", "Testowe10", -1)]
        public async Task CreateProductNoValidParametersTest(string code,string name,double price)
        {
            var product = new CreateProductCommand(code, name, price);

            var productsBefore = await _productRepository.GetAllAsync();
            var productsBeforeCount = productsBefore.Value.Count;

            var result = await _createProductHandler.Handle(product, CancellationToken.None);

            var productsAfter = await _productRepository.GetAllAsync();
            var productsAfterCount = productsAfter.Value.Count;

            result.ValidationErrors.Should().NotBeEmpty();
            result.Success.Should().BeFalse();
            result.Status.Should().Be(ResponseStatus.ValidationError);

            productsBeforeCount.Should().Be(productsAfterCount);
        }

        [Fact]
        public async Task CreateProductCodeIsExistTest()
        {
            var product = new CreateProductCommand("4124125125212", "Testowe7", 450);

            var productsBefore = await _productRepository.GetAllAsync();
            var productsBeforeCount = productsBefore.Value.Count;

            var result = await _createProductHandler.Handle(product, CancellationToken.None);

            var productsAfter = await _productRepository.GetAllAsync();
            var productsAfterCount = productsAfter.Value.Count;

            result.ValidationErrors.Should().BeEmpty();
            result.Success.Should().BeFalse();
            result.Status.Should().Be(ResponseStatus.ExistInDatabase);

            productsBeforeCount.Should().Be(productsAfterCount);
        }
    }
}
