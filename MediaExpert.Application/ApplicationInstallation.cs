﻿using Microsoft.Extensions.DependencyInjection;
using MediatR;
using System.Reflection;

namespace MediaExpert.Application
{
    public static class ApplicationInstallation
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            return services;
        }
    }
}
