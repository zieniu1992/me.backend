﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaExpert.Application.Mapper.Dtos
{
    public class IdsDto
    {
        public Guid Id { get; set; }
    }
}
