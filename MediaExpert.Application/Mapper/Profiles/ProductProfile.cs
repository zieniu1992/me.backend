﻿using AutoMapper;
using MediaExpert.Application.Functions.Products.Commands.CreateProduct;
using MediaExpert.Application.Mapper.Dtos;
using MediaExpert.Application.Mapper.Dtos.Products;
using MediaExpert.Domain.Entities.Products;

namespace MediaExpert.Application.Mapper.Profiles
{
    public class ProductProfile:Profile
    {
        public ProductProfile()
        {
            CreateMap<Product, IdsDto>();
            CreateMap<CreateProductCommand, Product>();
            CreateMap<Product,ProductDto>().ForMember(dest => dest.Created,
                                                      opt => opt.MapFrom(src => src.Created.ToUnixTimeMilliseconds()));
        }
    }
}
