﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaExpert.Application.Functions.Products.Commands.CreateProduct
{
    public class CreateProductCommand:IRequest<CreateProductCommandResponse>
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }

        public CreateProductCommand() { }

        public CreateProductCommand(string code, string name, double price)
        {
            Code = code;
            Name = name;
            Price = price;
        }
    }
}
