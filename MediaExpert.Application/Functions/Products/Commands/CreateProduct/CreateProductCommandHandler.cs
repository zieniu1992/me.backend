﻿using AutoMapper;
using MediaExpert.Application.Common;
using MediaExpert.Application.Interfaces.Persistence.Repositories.Products;
using MediaExpert.Application.Mapper.Dtos;
using MediaExpert.Domain.Entities.Products;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaExpert.Application.Functions.Products.Commands.CreateProduct
{
    public class CreateProductCommandHandler : IRequestHandler<CreateProductCommand, CreateProductCommandResponse>
    {
        private readonly IProductRepository _productRepository;
        private readonly IMapper _mapper;

        public CreateProductCommandHandler(IProductRepository productRepository, IMapper mapper)
        {
            _productRepository = productRepository;
            _mapper = mapper;
        }

        public async Task<CreateProductCommandResponse> Handle(CreateProductCommand request, CancellationToken cancellationToken)
        {
            var validator = new CreateProductCommandValidator();
            var validationResult = await validator.ValidateAsync(request);

            if (!validationResult.IsValid)
                return new CreateProductCommandResponse(validationResult);

            var checkProductResult = await _productRepository.GetProductByCodeAsync(request.Code);

            if (checkProductResult.Success)
                return new CreateProductCommandResponse(ResponseStatus.ExistInDatabase);

            var product = _mapper.Map<Product>(request);

            var createProductResult = await _productRepository.AddAsync(product);

            if (!createProductResult.Success)
                return new CreateProductCommandResponse(createProductResult.RemoveGeneric());

            var idsDto = _mapper.Map<IdsDto>(createProductResult.Value);

            return new CreateProductCommandResponse(idsDto);
        }
    }
}
