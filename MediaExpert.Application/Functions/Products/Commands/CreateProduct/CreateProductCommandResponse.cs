﻿using FluentValidation.Results;
using MediaExpert.Application.Common;
using MediaExpert.Application.Mapper.Dtos;
using MediaExpert.Domain.Status;

namespace MediaExpert.Application.Functions.Products.Commands.CreateProduct
{
    public class CreateProductCommandResponse : BaseResponse
    {
        public IdsDto Ids { get; set; }

        public CreateProductCommandResponse() : base()
        { }

        public CreateProductCommandResponse(ResponseStatus status)
            : base(status)
        {
        }

        public CreateProductCommandResponse(ExecutionStatus status)
            : base(status)
        {
        }

        public CreateProductCommandResponse(ExecutionStatus status, string message)
            : base(status, message)
        {
        }

        public CreateProductCommandResponse(ValidationResult validationResult)
            : base(validationResult)
        { }

        public CreateProductCommandResponse(string message)
        : base(message)
        { }

        public CreateProductCommandResponse(string message, bool success)
            : base(message, success)
        { }

        public CreateProductCommandResponse(IdsDto ids) : base()
        {
            Ids = ids;
        }
    }
}
