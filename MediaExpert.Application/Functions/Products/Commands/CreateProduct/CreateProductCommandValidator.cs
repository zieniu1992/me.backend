﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaExpert.Application.Functions.Products.Commands.CreateProduct
{
    public class CreateProductCommandValidator : AbstractValidator<CreateProductCommand>
    {
        public CreateProductCommandValidator()
        {
            RuleFor(s => s.Price).NotNull().GreaterThan(0);
            RuleFor(s => s.Code).NotNull().NotEmpty();
            RuleFor(s => s.Name).NotNull().NotEmpty();
        }
    }
}
