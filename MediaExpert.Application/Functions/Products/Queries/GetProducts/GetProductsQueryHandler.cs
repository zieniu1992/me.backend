﻿using AutoMapper;
using MediaExpert.Application.Interfaces.Persistence.Repositories.Products;
using MediaExpert.Application.Mapper.Dtos.Products;
using MediatR;

namespace MediaExpert.Application.Functions.Products.Queries.GetProducts
{
    public class GetProductsQueryHandler : IRequestHandler<GetProductsQuery, GetProductsQueryResponse>
    {
        private readonly IProductRepository _productRepository;
        private readonly IMapper _mapper;

        public GetProductsQueryHandler(IProductRepository productRepository, IMapper mapper)
        {
            _productRepository = productRepository;
            _mapper = mapper;
        }

        public async Task<GetProductsQueryResponse> Handle(GetProductsQuery request, CancellationToken cancellationToken)
        {
            var databaseResult = await _productRepository.GetAllAsync();

            if (!databaseResult.Success)
                return new GetProductsQueryResponse(databaseResult.RemoveGeneric());

            var productsDto = _mapper.Map<IReadOnlyList<ProductDto>>(databaseResult.Value);

            return new GetProductsQueryResponse(productsDto);
        }
    }
}
