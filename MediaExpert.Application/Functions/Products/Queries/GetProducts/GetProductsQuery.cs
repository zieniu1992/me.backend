﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaExpert.Application.Functions.Products.Queries.GetProducts
{
    public class GetProductsQuery:IRequest<GetProductsQueryResponse>
    {
    }
}
