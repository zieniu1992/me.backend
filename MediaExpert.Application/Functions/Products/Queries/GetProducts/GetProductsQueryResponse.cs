﻿using MediaExpert.Application.Common;
using MediaExpert.Application.Mapper.Dtos;
using MediaExpert.Application.Mapper.Dtos.Products;
using MediaExpert.Domain.Status;

namespace MediaExpert.Application.Functions.Products.Queries.GetProducts
{
    public class GetProductsQueryResponse : BaseResponse
    {
        public IReadOnlyList<ProductDto> Products { get; set; }

        public GetProductsQueryResponse(ResponseStatus status)
            : base(status)
        {
        }

        public GetProductsQueryResponse(ExecutionStatus status)
            : base(status)
        {
        }

        public GetProductsQueryResponse(ExecutionStatus status, string message)
            : base(status, message)
        {
        }

        public GetProductsQueryResponse(string message)
        : base(message)
        { }

        public GetProductsQueryResponse(string message, bool success)
            : base(message, success)
        { }

        public GetProductsQueryResponse(IReadOnlyList<ProductDto> products) : base()
        {
            Products = products;
        }
    }
}
