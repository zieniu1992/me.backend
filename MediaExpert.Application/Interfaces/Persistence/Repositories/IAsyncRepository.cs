﻿using MediaExpert.Domain.Status;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaExpert.Application.Interfaces.Persistence.Repositories
{
    public interface IAsyncRepository<T> where T : class
    {
        Task<ExecutionStatus<T>> AddAsync(T entity);

        Task<ExecutionStatus> DeleteAsync(T entity);

        Task<ExecutionStatus<IReadOnlyList<T>>> GetAllAsync();

        Task<ExecutionStatus<T>> GetByIdAsync(Guid id);

        Task<ExecutionStatus> UpdateAsync(T entity);
    }
}
