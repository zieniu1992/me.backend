﻿using MediaExpert.Domain.Entities.Products;
using MediaExpert.Domain.Status;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaExpert.Application.Interfaces.Persistence.Repositories.Products
{
    public interface IProductRepository:IAsyncRepository<Product>
    {
        Task<ExecutionStatus<Product>> GetProductByCodeAsync(string code);
    }
}
