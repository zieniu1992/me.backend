using System;
using FluentValidation.Results;
using MediaExpert.Domain.Status;

namespace MediaExpert.Application.Common
{
    public class BaseResponse
    {
        public ResponseStatus Status { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
        public List<string> ValidationErrors { get; set; }

        public WhatHTTPCodeShouldBeReturned WhatHTTPCodeToBeReturned
        {
            get
            {
                if (this.Status == ResponseStatus.BussinesLogicError)
                    return WhatHTTPCodeShouldBeReturned.BadRequest;
                if (this.Status == ResponseStatus.ValidationError ||
                    this.Status == ResponseStatus.BadQuery)
                    return WhatHTTPCodeShouldBeReturned.BadRequest;
                if (!this.Success)
                    return WhatHTTPCodeShouldBeReturned.MethodFailure;
                else
                    return WhatHTTPCodeShouldBeReturned.Ok;
            }
        }

        protected BaseResponse()
        {
            ValidationErrors = new List<string>();
            Success = true;
            Status = ResponseStatus.Success;
        }

        protected BaseResponse(string message = null)
        {
            ValidationErrors = new List<string>();
            Success = true;
            Message = message;
            Status = ResponseStatus.Success;
        }

        protected BaseResponse(ExecutionStatus status)
        {
            ValidationErrors = new List<string>();
            if (!status.Success)
            {
                Success = false;
                if (status.Where == WhereExecuted.DomainLogic)
                    Status = ResponseStatus.BussinesLogicError;
                if (status.Where == WhereExecuted.Database)
                    Status = ResponseStatus.DataBaseError;                
                Message = status.Message;
            }
            else
            {
                Success = true;
                Status = ResponseStatus.Success;
            }
        }

        protected BaseResponse(ExecutionStatus status, string message)
        {
            ValidationErrors = new List<string>();
            if (!status.Success)
            {
                Success = false;
                if (status.Where == WhereExecuted.DomainLogic)
                    Status = ResponseStatus.BussinesLogicError;
                if (status.Where == WhereExecuted.Database)
                    Status = ResponseStatus.DataBaseError;

                Message = message;
                Message += status.Message;
            }
            else
            {
                Success = true;
                Status = ResponseStatus.Success;
            }
        }

        protected BaseResponse(ResponseStatus status)
        {
            ValidationErrors = new List<string>();
            Success = status == ResponseStatus.Success;
            Status = status;
        }

        protected BaseResponse(string message, bool success)
        {
            ValidationErrors = new List<string>();
            Success = success;
            Message = message;
        }

        protected BaseResponse(ValidationResult validationResult)
        {
            ValidationErrors = new List<String>();
            Success = validationResult.Errors.Count <= 0;
            Status = validationResult.Errors.Count <= 0 ? ResponseStatus.Success : ResponseStatus.ValidationError;
            foreach (var item in validationResult.Errors)
            {
                ValidationErrors.Add(item.ErrorMessage);
            }
        }
    }
}
