using System;

namespace MediaExpert.Application.Common
{
    public enum ResponseStatus
    {
        Success = 0,
        BadQuery,
        ValidationError,
        ExistInDatabase,
        Error,
        DataBaseError,
        BussinesLogicError,
        Unauthorized
    }
}
