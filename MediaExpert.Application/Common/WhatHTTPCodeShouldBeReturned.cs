using System;

namespace MediaExpert.Application.Common
{
    public enum WhatHTTPCodeShouldBeReturned
    {
        Forbid,
        NotFound,
        BadRequest,
        MethodFailure,
        Ok
    }
}
