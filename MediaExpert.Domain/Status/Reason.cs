namespace MediaExpert.Domain.Status
{
  public enum Reason
    {
        None,
        Error,
        NotControledException,
        ReturnedNull,
    }
}