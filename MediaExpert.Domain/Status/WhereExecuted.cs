namespace MediaExpert.Domain.Status
{
    public enum WhereExecuted
    {
        Database = 0,
        DomainLogic = 1
    }
}