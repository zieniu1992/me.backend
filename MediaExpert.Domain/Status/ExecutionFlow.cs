namespace MediaExpert.Domain.Status
{
    public static class ExecutionFlow
    {
        public static IExecutionOptions Options { get; set; }

        static ExecutionFlow()
        {
            Options = new ExecutionOptions();
        }
    }
}