namespace MediaExpert.Domain.Status
{
    public interface IExecutionOptions
    {
        bool ThrowExceptions { get; set; }
    }

    public class ExecutionOptions : IExecutionOptions
    {
        public bool _throwExceptions;

        public ExecutionOptions()
        {

        }

        public ExecutionOptions(bool throwExceptions)
        {
            _throwExceptions = throwExceptions;
        }


        public bool ThrowExceptions
        {
            get => _throwExceptions;
            set => _throwExceptions = value;
        }
    }
}