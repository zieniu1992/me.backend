﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace MediaExpert.Api.Helpers
{
    [DefaultStatusCode(DefaultStatusCode)]
    public class MethodFailureResult : ObjectResult
    {
        private const int DefaultStatusCode = 420;

        /// <summary>
        /// Creates a new <see cref="NotFoundObjectResult"/> instance.
        /// </summary>
        /// <param name="value">The value to format in the entity body.</param>
        public MethodFailureResult([ActionResultObjectValue] object value)
            : base(value)
        {
            StatusCode = DefaultStatusCode;
        }
    }
}
