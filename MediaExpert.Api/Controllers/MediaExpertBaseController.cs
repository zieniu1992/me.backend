﻿using MediaExpert.Api.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;

namespace MediaExpert.Api.Controllers
{
    public abstract class MediaExpertBaseController:Controller
    {
        protected virtual MethodFailureResult MethodFailure([ActionResultObjectValue] object value)
        {
            return new MethodFailureResult(value);
        }
    }
}
