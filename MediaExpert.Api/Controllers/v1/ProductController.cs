﻿using MediaExpert.Application.Common;
using MediaExpert.Application.Functions.Products.Commands.CreateProduct;
using MediaExpert.Application.Functions.Products.Queries.GetProducts;
using MediaExpert.Application.Mapper.Dtos;
using MediaExpert.Application.Mapper.Dtos.Products;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MediaExpert.Api.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class ProductController:MediaExpertBaseController
    {
        private readonly IMediator _mediator;

        public ProductController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("addProduct",Name ="CreateNewProduct")]
        [ProducesResponseType(420)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<IdsDto>> Create([FromBody] CreateProductCommand createProduct)
        {
            if (createProduct == null)
                return BadRequest();

            var result = await _mediator.Send(createProduct);

            if (result.Status == ResponseStatus.ValidationError)
                return BadRequest();
            if (result.Status == ResponseStatus.ExistInDatabase)
                return Conflict();
            if (!result.Success)
                return MethodFailure(result.Message);

            return Ok(result.Ids);
        }

        [HttpGet("getAll", Name = "GetAllProducts")]
        [ProducesResponseType(420)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<ProductDto>>> GetAll()
        {
            var result = await _mediator.Send(new GetProductsQuery());

            if (!result.Success)
                return MethodFailure(result.Message);

            return Ok(result.Products);
        }
    }
}
