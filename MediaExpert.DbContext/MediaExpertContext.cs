﻿using MediaExpert.Domain.Entities;
using MediaExpert.Domain.Entities.Products;
using Microsoft.EntityFrameworkCore;

namespace MediaExpert.Persistence.Ef
{
    public class MediaExpertContext : DbContext
    {
        public MediaExpertContext()
        {

        }
        public MediaExpertContext(DbContextOptions<MediaExpertContext> options) : base(options)
        {
        }


        public DbSet<Product> Products { get; set; }

        #region Override save methods

        /// <summary>
        /// Synchroniczny zapis do bazy danych
        /// </summary>
        /// <returns></returns>
        public override int SaveChanges()
        {
            this.UpdateDate();
            return base.SaveChanges();
        }

        /// <summary>
        /// Asynchroniczny zapis do bazy danych
        /// </summary>
        /// <returns></returns>
        public async Task<int> SaveChangesAsync()
        {
            this.UpdateDate();
            return await base.SaveChangesAsync();
        }

        /// <summary>
        /// Aktualizacja daty modyfikacji
        /// </summary>
        private void UpdateDate()
        {
            var entries = ChangeTracker.Entries().Where(s => s.Entity is BaseEntity && (s.State == EntityState.Added || s.State == EntityState.Modified));

            foreach (var entry in entries)
            {
                if (entry.State == EntityState.Added)
                {
                    if (((BaseEntity)entry.Entity).Created <= DateTimeOffset.MinValue)
                    {
                        ((BaseEntity)entry.Entity).Created = DateTimeOffset.UtcNow;
                    }
                }

                ((BaseEntity)entry.Entity).Modified = DateTimeOffset.UtcNow;
            }
        }

        #endregion Override save methods

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.
                ApplyConfigurationsFromAssembly
                (typeof(MediaExpertContext).Assembly);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase(databaseName: "MediaExpertDb");
        }
    }
}
