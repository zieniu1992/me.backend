﻿using MediaExpert.Application.Interfaces.Persistence.Repositories;
using MediaExpert.Application.Interfaces.Persistence.Repositories.Products;
using MediaExpert.Persistence.Ef.Repositories;
using MediaExpert.Persistence.Ef.Repositories.Products;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace MediaExpert.Persistence.Ef
{
    public static class PersistenceEFInstallation
    {
        public static IServiceCollection AddPersistenceEF(this IServiceCollection services)
        {
            services.AddDbContext<MediaExpertContext>(options => options.UseInMemoryDatabase("MediaExpertDb"));

            services.AddScoped(typeof(IAsyncRepository<>), typeof(BaseRepository<>));
            services.AddScoped<IProductRepository, ProductRepository>();

            return services;
        }
    }
}
