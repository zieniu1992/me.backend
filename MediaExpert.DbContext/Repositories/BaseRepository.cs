﻿using MediaExpert.Application.Interfaces.Persistence.Repositories;
using MediaExpert.Domain.Status;
using Microsoft.EntityFrameworkCore;

namespace MediaExpert.Persistence.Ef.Repositories
{
    public class BaseRepository<T> : IAsyncRepository<T> where T : class
    {
        protected readonly MediaExpertContext _dbContext;

        public BaseRepository(MediaExpertContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual async Task<ExecutionStatus<T>> AddAsync(T entity)
        {
            try
            {
                await _dbContext.Set<T>().AddAsync(entity);
                await _dbContext.SaveChangesAsync();

                return ExecutionStatus<T>.DbOk(entity);
            }
            catch (Exception ex)
            {
                if (ExecutionFlow.Options.ThrowExceptions)
                    throw;

                return ExecutionStatus<T>.DbError(ex);
            }
        }

        public async Task<ExecutionStatus> DeleteAsync(T entity)
        {
            try
            {
                _dbContext.Set<T>().Remove(entity);
                await _dbContext.SaveChangesAsync();

                return ExecutionStatus.DbOk();
            }
            catch (Exception ex)
            {
                if (ExecutionFlow.Options.ThrowExceptions)
                    throw;

                return ExecutionStatus.DbError(ex);
            }
        }

        public async Task<ExecutionStatus<IReadOnlyList<T>>> GetAllAsync()
        {
            try
            {
                return ExecutionStatus<IReadOnlyList<T>>.DbIfDefaultThenError(await _dbContext.Set<T>().ToListAsync());
            }
            catch (Exception ex)
            {
                if (ExecutionFlow.Options.ThrowExceptions)
                    throw;

                return ExecutionStatus<IReadOnlyList<T>>.DbError(ex);
            }
        }

        public async Task<ExecutionStatus<T>> GetByIdAsync(Guid id)
        {
            try
            {
                var entity = await _dbContext.Set<T>().FindAsync(id);
                return ExecutionStatus<T>.DbIfDefaultThenError(entity);
            }
            catch (Exception ex)
            {
                if (ExecutionFlow.Options.ThrowExceptions)
                    throw;

                return ExecutionStatus<T>.DbError(ex);
            }
        }

        public async Task<ExecutionStatus> UpdateAsync(T entity)
        {
            try
            {
                _dbContext.Entry(entity).State = EntityState.Modified;
                await _dbContext.SaveChangesAsync();

                return ExecutionStatus.DbOk();
            }
            catch (Exception ex)
            {
                if (ExecutionFlow.Options.ThrowExceptions)
                    throw;

                return ExecutionStatus.DbError(ex);
            }
        }
    }
}
