﻿using MediaExpert.Application.Interfaces.Persistence.Repositories.Products;
using MediaExpert.Domain.Entities.Products;
using MediaExpert.Domain.Status;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaExpert.Persistence.Ef.Repositories.Products
{
    public class ProductRepository : BaseRepository<Product>, IProductRepository
    {
        public ProductRepository(MediaExpertContext dbContext) : base(dbContext)
        {
        }

        public async Task<ExecutionStatus<Product>> GetProductByCodeAsync(string code)
        {
            try
            {
                var product = await _dbContext.Products.FirstOrDefaultAsync(s => s.Code == code);

                return ExecutionStatus<Product>.DbIfDefaultThenError(product);
            }
            catch (Exception ex)
            {
                if (ExecutionFlow.Options.ThrowExceptions)
                    throw;

                return ExecutionStatus<Product>.DbError(ex);
            }
        }
    }
}
